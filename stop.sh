#!/usr/bin/env bash
cd ./AATT
TMP=$(forever list | grep aatt | wc -l)
if [ $TMP -gt 0 ] ; then
    forever stop aatt
fi